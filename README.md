### Hoot Smalltalk

![Hoot Owl][logo]

# PLEASE NOTE

[Hoot Smalltalk][hoot-home] has migrated its home to GitHub. 
This site has been _**DEPRECATED**_ and will no longer receive updates.
For all future developments of [Hoot Smalltalk][hoot-home], please see its new home page.

[logo]: hoot-design/hoot-owl.svg "Hoot Owl"
[hoot-home]: https://github.com/nikboyd/hoot-smalltalk#hoot-smalltalk
